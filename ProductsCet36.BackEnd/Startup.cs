﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ProductsCet36.BackEnd.Startup))]
namespace ProductsCet36.BackEnd
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
